package com.examen.javaFHibernate.modelo;

import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name="telefono")
public class Telefono {

	@Id
	private int id;
	@Column(name="id_est")
	private int id_est ;
	@Column(name="numero")
	private int numero;
//	@OneToMany(fetch=FetchType.LAZY, mappedBy="telefono", targetEntity=TelfDetalles.class)
//	private Set<TelfDetalles> telDetalles;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId_est() {
		return id_est;
	}
	public void setId_est(int id_est) {
		this.id_est = id_est;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}

}
