package com.examen.javaFHibernate.modelo;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name="materia_cursada")
public class MateriaCursada {

	@Id
	private int id;
//	@Column(name="est",nullable = false)
//	private int est ;
//	@Column(name="id_mat",nullable = false)
//	private int id_mat;
	@Column(name="calificacion")
	private Double calificacion;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="est", nullable=false)
	private Estudiante estudiante;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_mat", nullable=false)
	private Materia materia;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Double getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(Double calificacion) {
		this.calificacion = calificacion;
	}


}
