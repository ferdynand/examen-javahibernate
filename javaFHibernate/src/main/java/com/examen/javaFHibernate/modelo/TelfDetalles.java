package com.examen.javaFHibernate.modelo;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name="telf_detalles")
public class TelfDetalles {

	@Id
	private int id_t_det;
	@Column(name="proveedor",nullable = false)
	private String proveedor ;
	@Column(name="tipo")
	private String tipo;
	@Column(name="propietario")
	private String propietario;
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="id_t_det", nullable=false)
//	private Telefono telefono;
	public int getId_t_det() {
		return id_t_det;
	}
	public void setId_t_det(int id_t_det) {
		this.id_t_det = id_t_det;
	}
	public String getProveedor() {
		return proveedor;
	}
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getPropietario() {
		return propietario;
	}
	public void setPropietario(String propietario) {
		this.propietario = propietario;
	}

}
