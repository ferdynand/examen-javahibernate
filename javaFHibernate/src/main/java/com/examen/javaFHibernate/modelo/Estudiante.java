package com.examen.javaFHibernate.modelo;

import java.util.Set;

import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
@Entity
@Table(name="estudiante")
public class Estudiante {

	@Id
	@GenericGenerator(name="genId",strategy = "com.examen.javaFHibernate.generador.GeneraId")
	@GeneratedValue(generator = "genId")
	private int id;
	@Column(name="nombre")
	private String nombre;
	@Column(name="apellido")
	private String apellido;

	@OneToMany(fetch=FetchType.LAZY, mappedBy="estudiante", targetEntity=MateriaCursada.class)
	private Set<MateriaCursada> materiaCursada;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public Set<MateriaCursada> getMateriaCursada() {
		return materiaCursada;
	}
	public void setMateriaCursada(Set<MateriaCursada> materiaCursada) {
		this.materiaCursada = materiaCursada;
	}

}
