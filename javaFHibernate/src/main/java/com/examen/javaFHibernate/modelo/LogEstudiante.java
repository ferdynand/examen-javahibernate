package com.examen.javaFHibernate.modelo;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="log_estudiante")
public class LogEstudiante {

	@Id
	private int id_est;
	@Column(name="fecha_insert",nullable = false)
	private String fecha_insert;
	public int getId_est() {
		return id_est;
	}
	public void setId_est(int id_est) {
		this.id_est = id_est;
	}
	public String getFecha_insert() {
		return fecha_insert;
	}
	public void setFecha_insert(String fecha_insert) {
		this.fecha_insert = fecha_insert;
	}

}
