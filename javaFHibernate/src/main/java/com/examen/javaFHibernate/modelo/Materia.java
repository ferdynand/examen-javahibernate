package com.examen.javaFHibernate.modelo;

import java.util.Set;

import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name="materia")
public class Materia {

	@Id
	@GenericGenerator(name="genId2",strategy = "com.examen.javaFHibernate.generador.GeneraId")
	@GeneratedValue(generator = "genId2")
	private int id;
	@Column(name="sigla")
	private String sigla;
	@Column(name="descripcion")
	private String descripcion;
	@OneToMany(fetch=FetchType.LAZY, mappedBy="materia", targetEntity=MateriaCursada.class)
	private Set<MateriaCursada> materiaCursada;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
