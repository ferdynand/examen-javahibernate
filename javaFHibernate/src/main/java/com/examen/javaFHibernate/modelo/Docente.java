package com.examen.javaFHibernate.modelo;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="docente")
public class Docente {
	@Id
	private int id;
	@Column(name="nombr",nullable = false)
	private String nombr;
	@Column(name="apellido")
	private String apellido;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombr() {
		return nombr;
	}
	public void setNombr(String nombr) {
		this.nombr = nombr;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

}
