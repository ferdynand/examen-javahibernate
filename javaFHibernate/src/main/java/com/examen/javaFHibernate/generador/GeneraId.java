package com.examen.javaFHibernate.generador;

import java.io.Serializable;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

public class GeneraId implements IdentifierGenerator{
	@Override
	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
		Long id_l = System.currentTimeMillis();
    	String ids = id_l + "";
    	String id_aux = ids.substring(4, ids.length());
    	int id = Integer.parseInt(id_aux);
		return id;
	}
}
