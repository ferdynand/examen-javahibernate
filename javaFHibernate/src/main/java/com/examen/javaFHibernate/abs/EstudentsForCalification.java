package com.examen.javaFHibernate.abs;


public class EstudentsForCalification {
	private int id;
	private String nombre;
	private String apellido;
	private Double calificacion;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public Double getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(Double calificacion) {
		this.calificacion = calificacion;
	}
	public EstudentsForCalification(int id, String nombre, String apellido, Double calificacion) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.calificacion = calificacion;
	}
	public EstudentsForCalification() {
		super();
	}
	
}
