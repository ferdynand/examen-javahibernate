package com.examen.javaFHibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.examen.javaFHibernate.modelo.Estudiante;
import com.examen.javaFHibernate.modelo.MateriaCursada;
import com.examen.javaFHibernate.HibernateUtil;
import com.examen.javaFHibernate.abs.EstudentsForCalification;

/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args )
    {
//    	registrarEstudiante();
    	listarEstudiantes();
//    	listarEstudiantesOrdenandoPorNombre();
    }
    private static void registrarEstudiante() {
		Estudiante est = new Estudiante();
//    	est.setId(1);
		est.setNombre("Estudiante1");
		est.setApellido("Ape Estudent");

    	Transaction tx = null;
    	Session sesion = HibernateUtil.getSessionfactory().openSession();
    	try {
    		//Iniciar la sesión hibernate
    		tx = sesion.beginTransaction();
    		int numero = (Integer)sesion.save(est);
    		//Realizamos el commit
    		tx.commit();
    		System.out.println( "Se registró con éxito, id:" + numero );
    	}catch(HibernateException he) {
    		if(tx!=null)
    			tx.rollback();
    		he.printStackTrace();
    	}finally {
    		sesion.close();
    	}
	}
    private static void listarEstudiantes() {
    	Transaction tx = null;
    	Session sesion = HibernateUtil.getSessionfactory().openSession();
    	try {
    		//Iniciar la sesión hibernate
    		tx = sesion.beginTransaction();
    		
    		String hql = "FROM Estudiante";
    		Query query = sesion.createQuery(hql);
    		List<Estudiante>  estl= query.getResultList();
    		List<EstudentsForCalification> estCal = new ArrayList<EstudentsForCalification>();
    		for (Iterator iterator = estl.iterator(); iterator.hasNext();) {
				Estudiante est = (Estudiante) iterator.next();
//				System.out.println("Cantidad: " + est.getNombre());
				int cont = 0;
				for(Iterator iterador = est.getMateriaCursada().iterator(); iterador.hasNext();) {
	    			MateriaCursada v = (MateriaCursada) iterador.next();
//	    			System.out.println("Cantidad: " + v.getCalificacion());
	    			if(v.getCalificacion()>=80 && v.getCalificacion()<=90) {
	    				estCal.add(new EstudentsForCalification(est.getId(),est.getNombre(), est.getApellido(), v.getCalificacion()));
	    			}
	    		}
			}
    		for (Iterator iterator = estCal.iterator(); iterator.hasNext();) {
				EstudentsForCalification eFC = (EstudentsForCalification) iterator.next();
				System.out.println(eFC.getId() + " Nombre(s): " + eFC.getNombre() + " " + eFC.getApellido() + ", con calificacion entre 80 y 90 en cualquier materia");
			}
    		tx.commit();
    	}catch(HibernateException he) {
    		if(tx!=null)
    			tx.rollback();
    		he.printStackTrace();
    	}finally {
    		sesion.close();
    	}
	}
    private static void listarEstudiantesOrdenandoPorNombre() {
    	Transaction tx = null;
    	Session sesion = HibernateUtil.getSessionfactory().openSession();
    	try {
    		//Iniciar la sesión hibernate
    		tx = sesion.beginTransaction();
    		
    		String hql = "FROM Estudiante E ORDER BY E.nombre";
    		Query query = sesion.createQuery(hql);
    		List<Estudiante>  estl= query.getResultList();
    		List<EstudentsForCalification> estCal = new ArrayList<EstudentsForCalification>();
    		for (Iterator iterator = estl.iterator(); iterator.hasNext();) {
				Estudiante est = (Estudiante) iterator.next();
//				System.out.println("Cantidad: " + est.getNombre());
				int cont = 0;
				for(Iterator iterador = est.getMateriaCursada().iterator(); iterador.hasNext();) {
	    			MateriaCursada v = (MateriaCursada) iterador.next();
//	    			System.out.println("Cantidad: " + v.getCalificacion());
	    			if(v.getCalificacion()>=80 && v.getCalificacion()<=90) {
	    				estCal.add(new EstudentsForCalification(est.getId(),est.getNombre(), est.getApellido(), v.getCalificacion()));
	    			}
	    		}
			}
    		for (Iterator iterator = estCal.iterator(); iterator.hasNext();) {
				EstudentsForCalification eFC = (EstudentsForCalification) iterator.next();
				System.out.println(eFC.getId() + " Nombre(s): " + eFC.getNombre() + " " + eFC.getApellido() + ", con calificacion entre 80 y 90 en cualquier materia");
			}
    		tx.commit();
    	}catch(HibernateException he) {
    		if(tx!=null)
    			tx.rollback();
    		he.printStackTrace();
    	}finally {
    		sesion.close();
    	}
	}
}
